Accuracy: 0.6197718631178707

[[383  16]
 [284 106]]

              precision    recall  f1-score   support

   golf ball     0.5742    0.9599    0.7186       399
   parachute     0.8689    0.2718    0.4141       390

    accuracy                         0.6198       789
   macro avg     0.7215    0.6158    0.5663       789
weighted avg     0.7199    0.6198    0.5681       789

