from joblib import load
import json
from pathlib import Path

from sklearn.metrics import accuracy_score,confusion_matrix,classification_report,plot_confusion_matrix

from train import load_data
import matplotlib.pyplot as plt



def main(repo_path):
    test_csv_path = repo_path / "data/prepared/test.csv"
    test_data, labels = load_data(test_csv_path)
    model = load(repo_path / "model/model.joblib")
    predictions = model.predict(test_data)
    accuracy = accuracy_score(labels, predictions)
    matrix = confusion_matrix(predictions, labels)
    report = classification_report(predictions,labels)
    metrics = {"accuracy": accuracy}
    accuracy_path = repo_path / "metrics/accuracy.json"
    accuracy_path.write_text(json.dumps(metrics))

    disp = plot_confusion_matrix(model, test_data, labels, normalize='true',cmap=plt.cm.Blues)
    plt.savefig('confusion_matrix.png')

    with open("metrics.txt", 'w') as outfile:
        outfile.write("Accuracy: " + str(accuracy) + "\n\n")
        outfile.write(str(confusion_matrix(labels, model.predict(test_data))) + "\n\n")
        outfile.write(str(classification_report(labels, model.predict(test_data),digits=4)) + "\n")


if __name__ == "__main__":
    repo_path = Path(__file__).parent.parent
    main(repo_path)
